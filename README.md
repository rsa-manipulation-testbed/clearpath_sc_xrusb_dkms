# Repackaging of the Teknic Clearpath SC USB driver

The [Teknic Clearpath SC]() servo motor system has a USB interface.  Although compatible with the `cdc_acm` device driver, it requires a special driver to support all features.

This repo is a simple repackaging of the source from Teknic, primarily intended for repackaging as a DKMS package (see below). 

See [driver_readme.txt](driver_readme.txt) for the original README from Teknic.

For simple installation, `sudo ./Install_DRV_SCRIPT.sh`

# DKMS debian package

This repo can be used to build a `.deb` which uses DKMS to install the kernel module.   The `devscripts` package must be installed, then at the top level of the repo:

```
debuild -i -us -uc -b
```

**Note:** the `.deb` file will be generated _up one directory_.